﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Prueba_2.Startup))]
namespace Prueba_2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
